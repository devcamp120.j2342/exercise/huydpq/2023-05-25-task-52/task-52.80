import java.text.NumberFormat;
import java.util.Locale;

public class App {
    public static void main(String[] args) throws Exception {
        // task 1: Làm tròn n số sau dấu .
        double int1 = 2103440;
        int n = 2;
        System.out.format("Task 1: Số sau khi làm tròn là: %." + n + "f", int1);
        System.out.println();

        // task 2: Lấy 1 số random bất kỳ trong khoảng 2 số cho trước
        int max = 1;
        int min = 10;
        double random_double = Math.random() * (max - min + 1) + min;
        int int_random = (int) random_double;
        System.out.println("Task 2: " + int_random);

        // task 3: Tính số pytago từ 2 số đã cho c2 = a2 + b2
        // hàm Math.sprt (x) / x là tham số --/-- Trả về căn bậc hai của một số đã cho.
        int a3 = 2;
        int b3 = 4;
        double task3 = Math.sqrt(a3 * a3 + b3 * b3);
        System.out.println("Task 3: " + task3);

        // task 4: Kiểm tra số đã cho có phải số chính phương hay không c = a2
        int a4 = 64;
        int sqrt = (int) Math.sqrt(a4);
        if (sqrt * sqrt == a4) {
            System.out.println("Task 4: true");
        } else {
            System.out.println("Task 4: flase");
        }

        // task 5: Lấy số lơn hơn gần nhất chia hết cho 5
        int num5 = 23;
        double result5 = num5 - (num5 % 5) + 5;
        System.out.println("So lon hon gan nhat chia het cho 5 can tim: " + result5);

        // task 6: Kiểm tra đầu vào có phải số hay không

        System.out.println("\nTask 6:");
        Object[] inputs6 = { 12, "abcd", "12" };
        for (Object input : inputs6) {
            if (input instanceof Number) {
                System.out.println("True");
            } else if (input instanceof String) {
                try {
                    Integer.parseInt((String) input);
                    System.out.println("True");
                } catch (NumberFormatException e) {
                    System.out.println("Flase");
                }

            }
        }

        // Task 7: Kiểm tra số đã cho có phải lũy thừa của 2 hay không
        int a7 = 8;
        System.out.println("Task 7: " + task7(a7));

        // task 8: Kiểm tra số đã cho có phải số tự nhiên hay không
        double a8 = 1;
        System.out.println("Task 8: " + task8(a8));

        // task 9: Thêm dấu , vào phần nghìn của mỗi số 
        // tạo 1 NumberFormat để định dạng số theo tiêu chuẩn của nước Anh
        Locale localeEN = new Locale("en", "EN");
        NumberFormat en = NumberFormat.getInstance(localeEN);

        // đối với số có kiểu long được định dạng theo chuẩn của nước Anh
        // thì phần ngàn của số được phân cách bằng dấu phẩy
        double longNumber = 10000.23;
;
        String str1 = en.format(longNumber);
        System.out.println("Task 9: Số " + longNumber + " sau khi định dạng = " + str1);

        // task 10: Chuyển số từ hệ thập phân về hệ nhị phân
        int a10  = 4;
         System.out.println(Integer.toBinaryString(a10));

    }

    // Task 7: Kiểm tra số đã cho có phải lũy thừa của 2 hay không
    public static Boolean task7(int n) {
        if (n <= 0) {
            return false;
        }
        while (n % 2 == 0) {
            n = n / 2;
        }
        return n == 1;
    }

    // Task 8: Kiểm tra số đã cho có phải số tự nhiên hay không
    public static Boolean task8(double n) {
        return n > 0 && (int) n == n;
    }
}
